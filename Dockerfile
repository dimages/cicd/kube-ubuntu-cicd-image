FROM ubuntu:xenial

ENV PATH $PATH:/opt/google-cloud-sdk/bin

WORKDIR /

RUN apt update \
 && apt -qq -y upgrade \
 && apt -qq -y install ca-certificates openssh-client git gettext tar gzip python curl scrub coreutils gnupg \
 && echo "deb http://packages.cloud.google.com/apt cloud-sdk-xenial main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list \
 && curl https://packages.cloud.google.com/apt/doc/apt-key.gpg --output apt-key.gpg \
 && apt-key add apt-key.gpg \
 && rm apt-key.gpg \
 && apt update \
 && apt -qq -y install google-cloud-sdk kubectl \
 && curl https://raw.githubusercontent.com/helm/helm/master/scripts/get | bash \
 && apt -qq -y autoremove \
 && apt -qq clean \
 && apt -qq autoclean \
 && rm -rf /var/cache/apt/* /var/lib/apt/lists/*